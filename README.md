# A curated list of resources for community organizers

- [Do's and don'ts for conference organizers, a speaker's point-of-view](https://blog.frankel.ch/dos-donts-conference-organizers/)
- [Community Leadership Summit 2020 Recap](https://coghlan.me/2020/10/19/community-leadership-summit-recap)
- [A punk rock guide to conference organizing](https://increment.com/open-source/a-punk-rock-guide-to-conference-organizing/)
